# It may be required to run powershell as administrator and set execution policy to unrestricted. 

# Set-ExecutionPolicy Unrestricted

$User = Read-Host -Prompt 'Input the user name'

# CREATE DIRECTORIES
New-Item -Path C:\Users\$User\Documents\ -Name "_migration\_bookmarks" -ItemType "directory"
Write-Output "Migration and Bookmarks folders created in Documents"

# create chrome directory
New-Item -Path C:\Users\$User\Documents\_migration\_bookmarks\ -Name "_chrome" -ItemType "directory"
Write-Output "Chrome folder created"

# create explorer directory
New-Item -Path C:\Users\$User\Documents\_migration\_bookmarks\ -Name "_explorer" -ItemType "directory"
Write-Output "Explorer Folder Created"

#create old_desktop directory
New-Item -Path C:\Users\$User\Documents\_migration\_old_desktop\ -Name "_explorer" -ItemType "directory"
Write-Output "Old_desktop Folder Created"

# COPY BOOKMARKS

# Copy Chrome Bookmarks
Copy-Item "C:\Users\$User\AppData\Local\Google\Chrome\User` Data\Default\Bookmarks" -Destination C:\Users\$User\Documents\_migration\_bookmarks\_chrome

# Copy Edge Bookmarks
Copy-Item "C:\Users\$User\AppData\Local\Microsoft\Edge\User` Data\Default\Bookmarks" -Destination C:\Users\$User\Documents\_migration\_bookmarks\_explorer

# GATHER INFROMATION

# Get Virtual Drives
net use | Out-File -FilePath C:\Users\$User\Documents\virtual_drives.txt

Get-Printer | Out-File -FilePath C:\Users\$User\Documents\printers.txt

Write-Output $User | Out-File -FilePath C:\Users\$User\Documents\iberia_username.txt

Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate | Format-Table –AutoSize | Out-File -FilePath C:\Users\$User\Documents\_migration\installed_applications.txt

# Backup Desktop
Copy-Item -Path "C:\Users\$User\Desktop\*" -Destination "C:\Users\$User\Documents\_migration\_old_desktop\" -Recurse

# ToDo: Backup Desktop Folder To migration folder
    # Add a method to backup chrome and edge profiles
    # errorhandling of some sort for 
    #    - if the directory exits?
    #    - if the bookmarks files are not there 
